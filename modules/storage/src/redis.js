import {AccessDeniedError} from '@themost/common';
import * as redis from 'redis';
import {UserStorageService, UserStorageAccessConfiguration} from './service';

const REDIS_JSON_COMMANDS = ["json.del", "json.get", "json.mget", "json.set", "json.type",
    "json.numincrby", "json.nummultby", "json.strappend", "json.strlen", "json.arrappend", "json.arrindex",
    "json.arrinsert", "json.arrlen", "json.arrpop", "json.arrtrim", "json.objkeys", "json.objlen",
    "json.debug", "json.forget", "json.resp"];

function createJsonClient(options) {
    // add json commands
    REDIS_JSON_COMMANDS.forEach (command => {
        redis.addCommand(command);
    });
    // and finally return client
    return redis.createClient(options);
}


class RedisUserStorageService extends UserStorageService {

    /**
     * Formats a path like key string to a redis command compatible key e.g. user1/application1/lastAction to .user1.application1.lastAction
     * @param key
     * @returns {string|*}
     */
    static escapeKey(key) {
        let res = key.replace(/\//ig,'.');
        if (/^\./.test(res)) {
            return res;
        }
        return '.' + res;
    }

    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
    }

    /**
     * @name RedisClient#json_set
     * @param {*} ...args
     */

    /**
     * @name RedisClient#json_get
     * @param {*} ...args
     */

    /**
     *
     * @param {DataContext} context
     * @param {string} key
     * @param {*} value
     * @param {number=} expiration
     */
    async setItem(context, key, value, expiration) {
        // validate user
        const hasAccess = await context.getConfiguration().getStrategy(UserStorageAccessConfiguration).verify(context, `me/${key}`, 'write');
        if (hasAccess == null) {
            throw new AccessDeniedError('Write access to the specified user storage key is denied');
        }
        // solve Redis ReplyError: ERR new objects must be created at the root
        // when you are trying to create a new object at the root like application1
        let value_ = value;
        let key_ = key;
        // if key is a single object
        if (/^[A-zA-Z0-9_\\$]+$/ig.test(key)) {
            // set value with the specified key e.g. key=application1 value={ "foo": 100 } to { "application1": { "foo": 100 } }
            value_ = { };
            value_[key] = value;
            // set key to root
            key_ = ".";
        }
        // create redis client
        const client = createJsonClient(this.options);
        if (typeof expiration === "number" && expiration > 0) {
            // call json_set() with expiration
            return await new Promise((resolve, reject) => {
                client.json_set(`${context.user.name}`, UserStorageService.escapeKey(key_), 'EX', Math.ceil(expiration), JSON.stringify(value_), (err, res) => {
                    // quit client
                    client.quit(() => {
                        if (err) {
                            return reject(err);
                        }
                        // return data
                        return resolve(res);
                    });
                });
            });
        }
        // call json_set()
        return await new Promise((resolve, reject) => {
            client.json_set(`${context.user.name}`, UserStorageService.escapeKey(key_), JSON.stringify(value_), (err) => {
                // quit client
                client.quit(() => {
                    if (err) {
                        return reject(err);
                    }
                    // return data
                    return resolve(value);
                });
            });
        });
    }

    /**
     * @param {DataContext} context
     * @param {string} key
     */
    async getItem(context, key) {
        // validate user
        const hasAccess = await context.getConfiguration().getStrategy(UserStorageAccessConfiguration).verify(context, `me/${key}`, 'read');
        if (hasAccess == null) {
            throw new AccessDeniedError('Read access to the specified user storage key is denied');
        }
        // create redis client
        const client = createJsonClient(this.options);
        // call json_get()
        return await new Promise((resolve, reject) => {
            client.json_type(`${context.user.name}`, UserStorageService.escapeKey(key), (err, type) => {
                if (type == null) {
                    return client.quit(() => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(null);
                    });
                }
                client.json_get(`${context.user.name}`, UserStorageService.escapeKey(key), (err, res) => {
                    // quit client
                    client.quit(() => {
                        if (err) {
                            return reject(err);
                        }
                        // return data
                        if (res != null) {
                            return resolve(JSON.parse(res));
                        }
                        return resolve(res);
                    });
                });
            });

        });
    }

    /**
     * @param {DataContext} context
     * @param {string} key
     */
    async removeItem(context, key) {
        // validate user access
        const hasAccess = await context.getConfiguration().getStrategy(UserStorageAccessConfiguration).verify(context, `me/${key}`, 'write');
        if (hasAccess == null) {
            throw new AccessDeniedError('Write access to the specified user storage key is denied');
        }
        // create redis client
        const client = createJsonClient(this.options);
        // call json_del()
        return await new Promise((resolve, reject) => {
            client.json_del(`${context.user.name}`, UserStorageService.escapeKey(key), (err, res) => {
                // quit client
                client.quit(() => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(res === 1);
                });
            });
        });
    }

    /**
     * @param {DataContext} context
     */
    async clear(context) {
        // validate user access
        const hasAccess = context.getConfiguration().getStrategy(UserStorageAccessConfiguration).verify(context, `me`, 'write');
        if (hasAccess == null) {
            throw new AccessDeniedError('Write access to the specified user storage key is denied');
        }
        // create redis client
        const client = createJsonClient(this.options);
        // call json_del()
        return await new Promise((resolve, reject) => {
            client.del(`${context.user.name}`, (err, res) => {
                // quit client
                client.quit(() => {
                    if (err) {

                        return reject(err);
                    }
                    return resolve(res === 1);
                });
            });
        });
    }

}

module.exports.RedisUserStorageService = RedisUserStorageService;
