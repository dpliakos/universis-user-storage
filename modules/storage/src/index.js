import {UserStorageService, UserStorage, UserStorageAccessConfiguration, DefaultUserStorageAccessConfiguration} from './service';
import {MongoUserStorageService} from './mongo';
import {RedisUserStorageService} from './redis';

module.exports.UserStorageAccessConfiguration = UserStorageAccessConfiguration;
module.exports.DefaultUserStorageAccessConfiguration = DefaultUserStorageAccessConfiguration;
module.exports.UserStorageService = UserStorageService;
module.exports.UserStorage = UserStorage;
module.exports.MongoUserStorageService = MongoUserStorageService;
module.exports.RedisUserStorageService = RedisUserStorageService;
