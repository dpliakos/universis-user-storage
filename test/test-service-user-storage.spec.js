import request from 'supertest';
import {UserStorageService} from '../modules/storage/src/service';
import {MongoUserStorageService} from '../modules/storage/src/mongo';
import {assert} from 'chai';
import {ExpressDataApplication} from '@themost/express';
import app from './app/server/app';

describe('Test UserStorageService endpoints', () => {

    before((done) => {
        /**
         * @type {ExpressDataApplication}
         */
        const dataApplication = app.get(ExpressDataApplication.name);
        // set mongo configuration
        // set mongo configuration
        dataApplication.getConfiguration().setSourceAt('settings/universis/storage/options', {
            "host":"localhost",
            "port":27017,
            "database":"local_test",
            "authenticationDatabase": "admin",
            "user":"mongo",
            "password":"secret"
        });
        // use service
        dataApplication.useStrategy(UserStorageService, MongoUserStorageService);
        return done();
    });

    it('POST /api/users/me/storage/set', async ()=> {
        // set user configuration
        let response = await request(app)
            .post('/api/users/me/storage/set')
            .auth('admin', 'secret')
            .send(
                {
                    key: "registrar",
                    value: {
                        "lastDepartment": "1200",
                        "lastAction": "/requests/active"
                    }
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        assert.isObject(response.body);
        // get user configuration
        response = await request(app)
            .post('/api/users/me/storage/get')
            .auth('admin', 'secret')
            .set('Accept', 'application/json')
            .send(
                {
                    key: "registrar"
                }
            );
        assert.isObject(response.body);
        assert.equal(response.body.value.lastDepartment, "1200");
        // remove user configuration
        response = await request(app)
            .post('/api/users/me/storage/set')
            .auth('admin', 'secret')
            .send(
                {
                    key: "registrar",
                    value: null
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        // get user configuration again
        response = await request(app)
            .post('/api/users/me/storage/get')
            .auth('admin', 'secret')
            .set('Accept', 'application/json')
            .send(
                {
                    key: "registrar"
                }
            );
        assert.isObject(response.body);
        assert.isUndefined(response.body.value);
    });

    it('POST /api/users/me/storage/get VALUE', async ()=> {
        // set user configuration
        let response = await request(app)
            .post('/api/users/me/storage/set')
            .auth('admin', 'secret')
            .send(
                {
                    key: "registrar",
                    value: {
                        "lastDepartment": "1200",
                        "lastAction": "/requests/active"
                    }
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        response = await request(app)
            .post('/api/users/me/storage/get')
            .send(
                {
                    key: "registrar/lastDepartment"
                }
            )
            .auth('admin', 'secret')
            .set('Accept', 'application/json');
        assert.isObject(response.body);
        assert.equal(response.body.value, "1200");
        // remove user configuration
        response = await request(app)
            .post('/api/users/me/storage/set')
            .auth('admin', 'secret')
            .send(
                {
                    key: "registrar",
                    value: null
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
    });

    it('POST /api/users/me/storage/get ARRAY', async ()=> {
        // set user configuration
        let response = await request(app)
            .post('/api/users/me/storage/set')
            .auth('admin', 'secret')
            .send(
                {
                    key: "registrar",
                    value: {
                        "lastDepartment": "1200",
                        "lastActions": [ "/requests/active", "/requests/list" ]
                    }
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        response = await request(app)
            .post('/api/users/me/storage/get')
            .send(
                {
                    key: "registrar/lastActions"
                }
            )
            .auth('admin', 'secret')
            .set('Accept', 'application/json');
        assert.isObject(response.body);
        assert.isArray(response.body.value);
        assert.equal(response.body.value[1], "/requests/list");
        // remove user configuration
        response = await request(app)
            .post('/api/users/me/storage/set')
            .auth('admin', 'secret')
            .send(
                {
                    key: "registrar",
                    value: null
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
    });

});
