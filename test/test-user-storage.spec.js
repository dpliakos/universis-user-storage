import {UserStorageService, UserStorage} from '../modules/storage/src/service';
import {MongoUserStorageService} from '../modules/storage/src/mongo';
import {assert} from 'chai';
import {ExpressDataApplication} from '@themost/express';
import app from './app/server/app';
let User = require('./app/server/models/user-model');
describe('Test UserStorageService', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    before((done) => {
        /**
         * @type {ExpressDataApplication}
         */
        const dataApplication = app.get(ExpressDataApplication.name);
        // set mongo configuration
        dataApplication.getConfiguration().setSourceAt('settings/universis/storage/options', {
            "host":"localhost",
            "port":27017,
            "database":"local_test",
            "authenticationDatabase": "admin",
            "user":"mongo",
            "password":"secret"
        });
        // use service
        dataApplication.useStrategy(UserStorageService, MongoUserStorageService);
        // create context
        context = dataApplication.createContext();
        context.user = {
            name: 'admin',
            authenticationScope: 'registrar'
        };

        Object.defineProperty(User.prototype, 'storage', {
            get: function() {
                return UserStorage.create(context);
            },
            configurable: false
        });

        return done();
    });

    it('should set storage item', async () => {
        const user = await User.getMe(context);
        await user.storage.setItem('registrar', {
            lastDepartment: '101',
            lastPath: '/requests/list'
        });
        const registrar = await user.storage.getItem('registrar');
        assert.equal(registrar.lastDepartment, '101');
        assert.equal(registrar.lastPath, '/requests/list');
        await user.storage.removeItem('registrar');
    });

    it('should set storage item property', async () => {
        const user = await User.getMe(context);
        let result = await user.storage.setItem('registrar', {
            lastDepartment: '101'
        });
        const registrar = await user.storage.getItem('registrar');
        assert.equal(registrar.lastDepartment, '101');
        result = await user.storage.setItem('registrar/lastPath', '/requests/list');
        const lastPath = await user.storage.getItem('registrar/lastPath');
        assert.equal(lastPath, '/requests/list');
        await user.storage.removeItem('registrar');
    });


    it('should set nested item property', async () => {
        const user = await User.getMe(context);
        await user.storage.removeItem('registrar');
        await user.storage.setItem('registrar/tables/Table1/tableProperty', 100 );
        await user.storage.setItem('registrar/tables/Table2/tableProperty', 101 );
        const table1 = await user.storage.getItem('registrar/tables/Table1');
        const table2 = await user.storage.getItem('registrar/tables/Table2');
        assert.isDefined(table1);
        assert.isDefined(table2);
        await user.storage.removeItem('registrar');
    });

    it('should set storage item property array', async () => {
        const user = await User.getMe(context);
        let result = await user.storage.setItem('registrar', {
            lastDepartment: '101'
        });
        const registrar = await user.storage.getItem('registrar');
        assert.equal(registrar.lastDepartment, '101');
        result = await user.storage.setItem('registrar/lastPaths', [ '/requests/list', '/profile' ]);
        const lastPaths = await user.storage.getItem('registrar/lastPaths');
        assert.isArray(lastPaths);
        assert.equal(lastPaths[0], '/requests/list');
        await user.storage.removeItem('registrar');
    });

    it('should remove storage item property', async () => {
        const user = await User.getMe(context);
        let result = await user.storage.setItem('registrar', {
            lastDepartment: '101'
        });
        assert.equal(result.lastDepartment, '101');
        let registrar = await user.storage.getItem('registrar');
        assert.equal(registrar.lastDepartment, '101');
        result = await user.storage.setItem('registrar/lastPath', '/requests/list');
        result = await user.storage.removeItem('registrar/lastPath');
        registrar = await user.storage.getItem('registrar');
        assert.equal(registrar.lastDepartment, '101');
        assert.equal(registrar.lastPath, null);
        await user.storage.removeItem('registrar');
    });

    it('should throw access denied while getting storage item', async () => {
        const user = await User.getMe(context);
        return user.storage.getItem('application1')
            .then(
                () => Promise.reject(new Error('Expected method to reject.')),
                err => assert.instanceOf(err, Error)
            );
    });

});
