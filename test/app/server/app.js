
import express from 'express';
import path from 'path';
import { ExpressDataApplication, serviceRouter, dateReviver } from '@themost/express';
import indexRouter from './routes/index';
import authRouter from './routes/auth';
import passport from 'passport';
/**
 * @name Request#context
 * @description Gets an instance of ExpressDataContext class which is going to be used for data operations
 * @type {ExpressDataContext}
 */
/**
 * @name express.Request#context
 * @description Gets an instance of ExpressDataContext class which is going to be used for data operations
 * @type {ExpressDataContext}
 */

/**
 * Initialize express application
 * @type {Express}
 */
let app = express();

app.use(express.json({
  reviver: dateReviver
}));
app.use(express.urlencoded({ extended: false }));

// @themost/data data application setup
const dataApplication = new ExpressDataApplication(path.resolve(__dirname, 'config'));
// hold data application
app.set('ExpressDataApplication', dataApplication);

// use data middleware (register req.context)
app.use(dataApplication.middleware());
// use passport
app.use(authRouter(passport));
// use static content
app.use(express.static(path.resolve(process.cwd(), 'public')));

app.use('/', indexRouter);
// use @themost/express service router
app.use('/api', passport.authenticate('basic'), serviceRouter);

// error handler
app.use((req, res, next, err) => {
  // set locals, only providing error in development
  const locals = {
    message: err.message,
    error: req.app.get('env') === 'development' ? err : {},
  };
  // render the error page
  res.status(err.status || err.statusCode || 500);
  res.render('error', locals);
});

module.exports = app;
